#List of objects

#print all names
names = ['John','Bob','Mosh','Sam']
print(names) 

#single name 
print(names[2])

# -1 represents last element in Python and -2 represent sec to last

print(names[-1])
print(names[-2])

#update list
names[0] = 'Jon'
print(names)

#Range of index
print(names[0:3])

#does not change original list. Stores list to a new list
print(names)


