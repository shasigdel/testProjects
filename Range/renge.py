numbers = range(5)
#first value is 5 and last value is 10
numbers_two =  range(5, 10)
#range with skip number 2
numbers_three = range(5, 10, 2)
print (numbers)

for number in numbers:
    print(number)

for number in numbers_two:
    print(number)

for number in numbers_three:
    print(number)